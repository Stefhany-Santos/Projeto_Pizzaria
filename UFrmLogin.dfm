inherited FrmLogin: TFrmLogin
  AlphaBlend = True
  AlphaBlendValue = 245
  Caption = 'Mensagens'
  ClientHeight = 413
  ClientWidth = 500
  Position = poDefault
  WindowState = wsMaximized
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  ExplicitWidth = 500
  ExplicitHeight = 413
  PixelsPerInch = 96
  TextHeight = 17
  inherited PnlTitulo: TPanel
    Width = 500
    Height = 403
    Align = alClient
    ExplicitWidth = 500
    ExplicitHeight = 403
  end
  inherited PnlRodape: TPanel
    Top = 403
    Width = 500
    Visible = False
    ExplicitTop = 403
    ExplicitWidth = 500
  end
  inherited PnlGeral: TPanel
    Width = 480
    Height = 390
    Align = alNone
    Anchors = []
    Color = clBlack
    Font.Color = clWhite
    ParentFont = False
    ExplicitWidth = 480
    ExplicitHeight = 390
    object Label1: TLabel
      Left = 41
      Top = 0
      Width = 33
      Height = 86
      Caption = ':)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -64
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 41
      Top = 84
      Width = 67
      Height = 37
      Caption = 'Login'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -27
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 36
      Top = 156
      Width = 151
      Height = 25
      Caption = 'Nome de '#218'suario:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 36
      Top = 220
      Width = 145
      Height = 25
      Caption = 'Senha de Acesso:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Bevel1: TBevel
      Left = 36
      Top = 127
      Width = 384
      Height = 7
      Shape = bsTopLine
    end
    object BtnCancelar: TSpeedButton
      Left = 240
      Top = 288
      Width = 180
      Height = 22
      Caption = 'Cancelar'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = BtnCancelarClick
    end
    object BtnEntrar: TSpeedButton
      Left = 88
      Top = 288
      Width = 180
      Height = 22
      Caption = 'Entrar'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = BtnEntrarClick
    end
    object EdtNome: TEdit
      Left = 217
      Top = 159
      Width = 176
      Height = 33
      BevelInner = bvNone
      BevelOuter = bvNone
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Text = 'EdtNome'
    end
    object EdtSenha: TEdit
      Left = 217
      Top = 220
      Width = 176
      Height = 33
      BevelInner = bvNone
      BevelOuter = bvNone
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Text = 'EdtSenha'
    end
  end
end

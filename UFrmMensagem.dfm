inherited FrmMensagem: TFrmMensagem
  Top = 307
  AlphaBlend = True
  AlphaBlendValue = 245
  Caption = 'Mensagens'
  ClientHeight = 364
  ClientWidth = 865
  Position = poDesigned
  WindowState = wsMaximized
  OnShow = FormShow
  ExplicitWidth = 865
  ExplicitHeight = 364
  PixelsPerInch = 96
  TextHeight = 17
  inherited PnlTitulo: TPanel
    Width = 865
    Height = 354
    Align = alClient
    ExplicitWidth = 865
    ExplicitHeight = 354
  end
  inherited PnlRodape: TPanel
    Top = 354
    Width = 865
    Visible = False
    ExplicitTop = 354
    ExplicitWidth = 865
  end
  inherited PnlGeral: TPanel
    Width = 838
    Height = 343
    Align = alNone
    Anchors = []
    Color = clBlack
    Font.Color = clWhite
    ParentFont = False
    ExplicitWidth = 838
    ExplicitHeight = 343
    object LblTipo: TLabel
      Left = 41
      Top = 0
      Width = 33
      Height = 86
      Caption = ':)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -64
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object LblTitulo: TLabel
      Left = 41
      Top = 92
      Width = 770
      Height = 37
      AutoSize = False
      Caption = 'T'#237'tulo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -27
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object LblMensagem: TLabel
      Left = 41
      Top = 148
      Width = 770
      Height = 97
      AutoSize = False
      Caption = 'Mensagem...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -21
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object LblTempo: TLabel
      Left = 28
      Top = 269
      Width = 384
      Height = 20
      Caption = 'Esta janela ser'#225' fechada automaticamente em 5 segundos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Bevel1: TBevel
      Left = 41
      Top = 135
      Width = 760
      Height = 7
      Shape = bsTopLine
    end
    object Btn1: TSpeedButton
      Left = 704
      Top = 256
      Width = 120
      Height = 22
      Caption = 'OK'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = Btn1Click
    end
    object Btn2: TSpeedButton
      Left = 578
      Top = 256
      Width = 120
      Height = 22
      Caption = 'Sim'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = Btn2Click
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 776
    Top = 94
  end
end

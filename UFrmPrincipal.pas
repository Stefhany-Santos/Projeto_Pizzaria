unit UFrmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFrmBase, Vcl.ExtCtrls, Vcl.Buttons,
  Vcl.StdCtrls, UFrmMensagem;

type
  TFrmPrincipal = class(TFrmBase)
    BtnFechar: TSpeedButton;
    BtnOK: TButton;
    BtnErro: TButton;
    BtnPergunta: TButton;
    BtnTempo: TButton;
    procedure BtnFecharClick(Sender: TObject);
    procedure BtnOKClick(Sender: TObject);
    procedure BtnErroClick(Sender: TObject);
    procedure BtnPerguntaClick(Sender: TObject);
    procedure BtnTempoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmPrincipal: TFrmPrincipal;

implementation

{$R *.dfm}

procedure TFrmPrincipal.BtnFecharClick(Sender: TObject);
begin
  Close
end;

procedure TFrmPrincipal.BtnErroClick(Sender: TObject);
begin
  FrmMensagem.Mensagem('Problemas ao gravar', 'Informe todos os dados do produto para completar o cadastro', msgErro);
end;

procedure TFrmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if  FrmMensagem.Mensagem('Confirma o fechamento do Sistema?', '', msgSimNao, 5) = mrNo then
  Abort;

end;

procedure TFrmPrincipal.BtnOKClick(Sender: TObject);
begin
  FrmMensagem.Mensagem('Problemas ao gravar', 'Informe todos os dados do produto para completar o cadastro', msgErro);
end;

procedure TFrmPrincipal.BtnPerguntaClick(Sender: TObject);
begin
   if (FrmMensagem.Mensagem('Gostou da Mensagem?', 'Esta mensagem personalizada � um patrocinio Gallex Coders', msgSimNao) = mrYes) then
      FrmMensagem.Mensagem('Uhull', 'Que bom deu trabalho para fazer tudo isso!!', msgOK)
   else FrmMensagem.Mensagem('Poxa', 'Tudo bem, vou tentar melhorar para a pr�xima!', msgErro);
end;

procedure TFrmPrincipal.BtnTempoClick(Sender: TObject);
begin
  FrmMensagem.Mensagem('Cadastro Conclu�do', 'Esta mensagem � apenas inform�tiva e ser� fechada automaticamente', msgOK, 10);

end;
end.

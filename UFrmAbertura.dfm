inherited FrmAbertura: TFrmAbertura
  Caption = ' Pizzaria Etec'
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 17
  inherited PnlTitulo: TPanel
    Alignment = taCenter
    Caption = 'Pizzaria Etec'
    Font.Color = clWhite
  end
  inherited PnlGeral: TPanel
    ExplicitTop = 72
    object Label1: TLabel
      Left = 192
      Top = 104
      Width = 100
      Height = 25
      Caption = 'Carregando'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 298
      Top = 104
      Width = 12
      Height = 25
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object ProgressBar1: TProgressBar
      Left = 192
      Top = 135
      Width = 305
      Height = 17
      TabOrder = 0
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 200
    OnTimer = Timer1Timer
    Left = 464
    Top = 118
  end
end

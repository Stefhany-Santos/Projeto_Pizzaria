unit UFrmMensagem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFrmBase, Vcl.ExtCtrls, Vcl.Buttons,
  Vcl.StdCtrls;

type
   TTipo = (msgOK, msgERRO, msgSimNao);

TFrmMensagem = class(TFrmBase)
    LblTipo: TLabel;
    LblTitulo: TLabel;
    LblMensagem: TLabel;
    LblTempo: TLabel;
    Bevel1: TBevel;
    Btn1: TSpeedButton;
    Timer1: TTimer;
    Btn2: TSpeedButton;
    procedure Btn2Click(Sender: TObject);
    procedure Btn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    tipoMensagem : TTipo;
    tempo : integer;
public
    { Public declarations }
    function Mensagem(Titulo : string; Texto : string; Tipo : TTipo) : TModalResult; overload;
    function Mensagem(Titulo : string; Texto : string; Tipo : TTipo; Tempo: integer) : TModalResult; overload;
end;

var
  FrmMensagem: TFrmMensagem;

implementation

{$R *.dfm}

{ TFrmMensagem }

function TFrmMensagem.Mensagem(Titulo, Texto: string; Tipo: TTipo): TModalResult;
begin
  tipoMensagem := Tipo;
  if (tipo = msgOK) or (Tipo = msgSimNao) then
  Lbltipo.Caption := ':)'
  else
  Lbltipo.Caption := ':(';

  LblTitulo.Caption := Titulo;
  LblMensagem.Caption := Texto;

  if (Tipo = msgOk) or (Tipo = msgErro) then
  begin
  btn2.Visible := False; // oculta o botao do Sim
  btn1.Caption := 'OK';
  end
  else
  begin
  btn2.Visible := True; // mostra o bot�o do sim
  btn1.Caption := 'N�o';
  end;

  Mensagem := Self.ShowModal;
  Timer1.Enabled := false;
  LblTempo.Visible := false;
end;


procedure TFrmMensagem.Btn1Click(Sender: TObject);
begin
  if  tipoMensagem = msgOk then
  Close
  else
    ModalResult := mrNo;
end;

procedure TFrmMensagem.Btn2Click(Sender: TObject);
begin
  ModalResult := mrYes;
end;

procedure TFrmMensagem.FormShow(Sender: TObject);
begin
     PnlGeral.Left := (Self.Width - PnlGeral.Width) div 2;
     PnlGeral.Top := (Self.Height - PnlGeral.Height) div 2;
end;

function TFrmMensagem.Mensagem(Titulo, Texto: string;
  Tipo: TTipo; Tempo: integer): TModalResult;
begin
    LblTempo.Caption:= 'Esta janela ser� fechada automaticamente em' + IntToStr(Tempo) + ' Segundos';
    self.tempo := Tempo;
    Timer1.Enabled := true;
    LblTempo.Visible := true;
    Mensagem := Mensagem(Titulo, Texto, Tipo);
end;

procedure TFrmMensagem.Timer1Timer(Sender: TObject);
begin
    dec(self.tempo);
    LblTempo.Caption:= 'Esta janela ser� fechada automaticamente em ' + IntToStr(tempo);
    if (tempo > 1) then
    LblTempo.Caption := LblTempo.Caption + ' segundos'
    else
    LblTempo.Caption := LblTempo.Caption + ' segundo';

if (self.tempo = 1) then
begin
    Timer1.Enabled := false;
    btn1.Click;
end;
end;
end.

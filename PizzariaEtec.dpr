program PizzariaEtec;

uses
  Vcl.Forms,
  UFrmBase in 'UFrmBase.pas' {FrmBase},
  UFrmPrincipal in 'UFrmPrincipal.pas' {FrmPrincipal},
  UFrmAbertura in 'UFrmAbertura.pas' {FrmAbertura},
  UFrmMensagem in 'UFrmMensagem.pas' {FrmMensagem},
  UFrmLogin in 'UFrmLogin.pas' {FrmLogin};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  Application.CreateForm(TFrmMensagem, FrmMensagem);
  Application.CreateForm(TFrmAbertura, FrmAbertura);
  Application.CreateForm(TFrmLogin, FrmLogin);
  FrmAbertura.ShowModal;
  FrmLogin.ShowModal;
  Application.Run;
end.

unit UFrmLogin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFrmBase, Vcl.ExtCtrls, Vcl.Buttons,
  Vcl.StdCtrls;

type
  TFrmLogin = class(TFrmBase)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Bevel1: TBevel;
    EdtNome: TEdit;
    EdtSenha: TEdit;
    BtnCancelar: TSpeedButton;
    BtnEntrar: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnEntrarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
  private
    { Private declarations }
    logou : boolean;
  public
    { Public declarations }
  end;

var
  FrmLogin: TFrmLogin;

implementation

{$R *.dfm}

procedure TFrmLogin.BtnCancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TFrmLogin.BtnEntrarClick(Sender: TObject);
begin
    logou := true;
    Close;
end;

procedure TFrmLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if not logou then
   Application.Terminate
end;

procedure TFrmLogin.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
     btnEntrar.Click;
end;

procedure TFrmLogin.FormShow(Sender: TObject);
begin
   logou := false;
   PnlGeral.Left := (Self.Width - PnlGeral.Width) div 2;
   PnlGeral.Top := (Self.Height - PnlGeral.Height) div 2;
end;

end.
